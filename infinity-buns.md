[[_TOC_]]

# Quick version

1. 7.5 cups flour (4 wheat, 3.5 wheat)
2. 4.5 cups cooked sweet potato (4 cups if using mushrooms)
3. 3 cups fresh mushrooms (optional)
4. 2 cups of cooked, toasted quinoa
5. 2 tbsp honey
6. thyme, oregano, onion
7. combine above + sit for 20 minutes (autolyse)
8. combine 2 tbs warm water + 2.5 tsp active dry yeast + some honey.  Set for 20 minutes

9. to flour mix, add
10. 1 stick of butter
11. 2.5 tsp salt
12. Mix thoroughly
13. Add yeast, mix thoroughly
14. knead for 20 minutes
15. Cover + proof for 1 hour
16. Knead + form bread
17. Place buns on cooking sheet(s) w/ small amount of corn meal under (to prevent sticking)
18. Cover + proof at room temp for 1.5 hours
19. Turn on oven to 350, cook for 42 minutes (or 35 minutes if you pre-heat the oven)


# Infinity buns recipe

> These buns are infinitely good, healthy, and useful with both savory and sweet options.  Many ways to mod them once you get the hang.
> * Total time: 4hrs
> * Human effort: 1.5hrs

**Why this recipe?** - This recipe attempts to pack as much nutritional stuff together as possible, while still producing the tastiest bun.  Some specific features:
- **Reduce / eliminate water addition** in favor of adding foods w/ water in them (sweet potato)
- **Kid friendly** - Rolls are great for PBJ or just better, heat up in 45s from freezer perfectly
- **Packed with healthy stuff** that doesnt' interfere.  Contains dried mushrooms, sweet potatoes, quinoa, and 2/3 whole wheat!
- **Modifiable** so you'll know what is adjustable and what is not

For reference, this recipe started here: https://www.kingarthurbaking.com/recipes/honey-wheat-rolls-recipe.

## 1 Prepare ingredients

### 1.1 Prepare animal drippings
_optional, but really adds more flavor_

- drippings from hamburgers, cooked chicken... anything like that

Take your drippings and put them in the fridge.  They should separate into a hardened fat layer (on top) and a water-soluble later (on bottom).  You can (optionally) use these later in the recipe.

### 1.2 Dry mushrooms
_optional, but adds flavor and nutritional value_

- 2 or more cups of any mushrooms thinly sliced, mixed varieties are better but any will do

Thinly slice mushrooms and place evenly tray in oven, top rack, at 400F.  Check every 10 minutes.  Once dry (crackly), put them in coffee grinder to grind into a flour (consistency of corn meal is fine).

### 1.3 Cook quinoa
_Toasting quinoa is waaaaaay better and adds a nutty rather than grain bin-ey flavor.  You'll never go back :)_

- (optional) animal drippings (fat portion)
- Water
- 2 cups dry quinoa
- rice cooker (or stove if you don't have one)

Heat pan to med-low, add dry (uncooked) quinoa.  Quinoa will begin to 'pop' as the grains toast.  Mix occassionally to keep it from burning.  Once you notice it's mostly browned and you smell the nutty flavor, put it in your rice cooker on the 'white rice - quick cook' setting, or just cook on stove.  Add the drippings before cooking, these will add savory flavor in the end.  You can add the non-fat (stock) portion, but be aware this can cause your rice cooker to bubble over.

### 1.4 Cook sweet potatoes

- 2.5 cups sweet potatoes (1/2cm cubes)

Cook sweet potatoes in hot water on stove.  Make sure they are mushy (fully done).  Drain water and mash completely, ensuring they don't have a chance to dry out (keep them covered) because the water is important for the bread recipe.  You can also bake your sweet potatoes whole in the oven, but just don't bake them for too long to lose the water.

### 1.5 Prepare [tangzhong](https://www.kingarthurbaking.com/blog/2018/03/26/introduction-to-tangzhong)
_Tangzhong helps the bread keep it's freshness days longer than normal bread_

- 1 cup water
- 1/2 cup white flour

Mix in a small pot, heat on low until you form a rue (stir frequently to not overcook the flour).  Set aside once complete.

## 2 Combine ingredients for [autolyse](https://www.kingarthurbaking.com/blog/2017/09/29/using-the-autolyse-method)
_Autolysing improves the quality of the bread structure.  Helpful here as this bread has lots of non-flour stuff in it (ie enriched) which can affect structure + bake_

- 2 cups white flower (sifted)
- 4 cups wheat flower (sifted)
- Tangzhong rue (from above)
- Mashed sweet potatoes (from above)
- Dried mushroom flour (from above)
- Toasted/cooked quinoa (from above)

Mix thoroughly, use your fingers to smush up wet ingredients into the flour mixture.  It will still be chunky, but most of the wet should be interfacing with the dry.  Cover bowl with plastic and place in oven on 'proof' setting, or in place which is warm.

- Wait 30 minutes

### 2.1 Start yeast

- 2 tsp active dry yeast
- 4 tbs water OR
- 4 tbs animal drippings / stock (water-soluble portion)
- pinch of sugar

Mix yeast and water or water-soluble portion of animal drippings (or stock).  Yeast doesn't absorb water well, so make sure it's thoroughly mixed.  Wait 15 minutes until you see it bubble.  Ideally do this after 15 minutes of the previous step is complete.

### 2.2 Mix in butter or avacados and salt

- Autolysed mixture from (2.1) above
- 1/2 cup (1 stick) chopped butter (1/8'' squares) OR
- 1/2 cup chopped avocados (1/8'' squares)
- 2.5 tsp salt
- yeast mixture from (2.2) above

Add butter/avacados and salt to autolysed mixture and mix by hand.  **Note** Avocado's don't change the taste very much, but they will slightly reduce the rise (still pretty good and fluffy tho)  Don't form dough (squish together), just fluff material around.  This step must be done before adding yeast as concentrated salt can deactivate yeast.  Once mixed, add the yeast, further mix enough to incorporate materials, and begin forming dough.

## 3 Knead Dough, let rise

Knead by hand for 10 - 12 minutes.  Use wheat flour on table if sticking.  Add just enough flour to keep it from being a mess.  Being too generous with extra flour here will reduce the rise and make it too dense.  Generally, sticky is better than hard.

Once complete, form into ball and set in bowl.  Cover in lose plastic (lose is important - if the plastic pushes against the dough it'll prevent rise).  Place in oven on 'proof' setting for 1 hour, or in warm area (70F minimum) for 15 hours.  Dough should double in size.

## 4 Cut into buns, 2nd rise

Prepare a baking sheet by lightly dusting the bottom with corn meal.

Remove the dough, place on table.  Punch out air and cut into 8 equal sections.  Form those sections into balls, then cut each ball in half.  You now have 16 bun sized dough balls.

Knead each ball for 5 - 10 seconds, giving more material for the yeast to eat and helping the structure become more consistent.  Roll them into tight balls and place them on the pan.  You can put them close to each other - they'll grow into each other and be yummy.  I like 4 x 4 in the pan.

Put the pan in a plastic bag large enough that the dough can rise without touching the bag (important!).  Make sure the bag is pretty airtight.

Place back in the oven on the proof setting for 1 hour, or at 70F+ for 1.5 hours.

## 5 Bake

- melter butter or egg wash
- salt

Wash the buns with melter butter or egg wash for a shiney finish, and sprinkle salt on top.  Do this while they are still in the oven on the 'proof' setting to keep them fluffy.

Set a timer to 41 minutes.  Set the oven to 350 and hit start.  Place the tray + buns in the oven **as the oven is preheating**.  I find that if the buns cool even a little, they will shrink.  By letting them heat up as the oven heats up, they stay fluffy.

## 6. Cool + Freeze

For some reason, these buns are better after freezing.  Once out of the oven, separate the buns to let them cool.  As soon as they are room temperature and aren't giving off lots of moisture, place them in the freezer on a tray or just in the freezer bare.  Once they freeze, you can put them in a bag for long-term storage.  

To eat - just take them out of the freezer, microwave for 45s and poof - amazing, healthy buns!

** 7. More mods???

- We add 1 cup water to the tangzhong... maybe replace that with veggie or bone broth?  Other healthy liquid?  Mashed up fruit (fruit is 90%ish water)...
- Could try 100% whole wheat flour (except the 1/2 cupt the tangzhong... it doesnt' work without it).
- Make these mini-meat pies!!!! Add yurmy meat filling to the middle of the bun before 2nd rise, onions, garlic, ground beef, seasoning... I dunno!
- Replace quinoa with some other cooked grain?  Just needs to be not too crunchy, something smoothish that doesn't have an overwhelming flavor.
- Could we make this wheat and gluten free?  Perhaps using beer as the rising agent, or just old fashioned baking powder?  That would make it more like irish soda break, but still could be good.
- Could make separate savory and sweet versions... sweet versions could use honey and/or non-sugar sweetner ([Purecane is amazing](https://purecane.com/?utm_source=google&utm_medium=cpc&utm_campaign=Brand+l_DE1+60eb&utm_term=111695574439-kwd-901121408140&utm_content=477375223323&gclid=CjwKCAiA4rGCBhAQEiwAelVti3osiPHZx0BNty9qFfHniYbYTH5JmqEmsBiVU8BrwIBIg7Xsg05A5hoCnjUQAvD_BwE) or erythretol / monkfruit.

